// Vendors

import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/forms';
import '@angular/http';
import '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

import 'jquery';
import 'tether';
import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/scss/bootstrap-flex.scss';


if ('production' === ENV) {
    // Production

} else {
    // Development

}