import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { routedComponents, LoginRoutingModule } from './login-routing.module';
import { UserService } from '../shared/user.service';
import { AuthenticationService } from '../shared/authentication.service';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        LoginRoutingModule
    ],
    declarations: [ routedComponents ],
    providers: [
        UserService,
        AuthenticationService
    ]
})
export default class LoginModule { }