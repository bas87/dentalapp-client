import { NgModule }       from '@angular/core';
import { RouterModule }   from '@angular/router';
import { LoginComponent } from './login.component';


export const routedComponents = [LoginComponent];


@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: LoginComponent }
        ])
    ],
    exports: [ RouterModule ]
})
export class LoginRoutingModule { }