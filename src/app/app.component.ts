import { Component, ViewEncapsulation } from '@angular/core';


@Component({
    selector: 'dental-app',
    encapsulation: ViewEncapsulation.None,
    styles: ['body { background-color: #eee; }'],
    template: `
        <router-outlet></router-outlet>
    `
})
export class AppComponent { }