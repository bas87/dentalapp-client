import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NoContentComponent } from './no-content';


@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: '', loadChildren: './base' },
            { path: 'login', pathMatch: 'full', loadChildren: './login' },
            { path: '**', component: NoContentComponent }
        ])
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }