import {Diagnosis} from "./diagnosis.model";
import {Product} from "./product.model";

export class DiagnosisProduct {
    constructor(
        public diagnosis: Diagnosis,
        public product: Product,
        public localization: string,
        public amount: number
    ) {};
}