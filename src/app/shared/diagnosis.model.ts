export class Diagnosis {
    constructor(
        public id: number,
        public code: string,
        public name: string
    ) {};
}