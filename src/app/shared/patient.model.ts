import {InsuranceType} from './insurance-type.model';
import {InsuranceCompany} from './insurance-company.model';

export class Patient {
    constructor(
        public id: number,
        public insuranceType: InsuranceType,
        public insuranceCompany: InsuranceCompany,
        public firstname: string,
        public lastname: string,
        public photo: string,
        public birthdate: string,
        public rc: string,
        public sex: string,
        public phoneNumber: string,
        public email: string,
        public street: string,
        public city: string,
        public zip: string,
        public insurenceNumber: string,
        public medicalHistory: string,
        public allergy: string,
        public drugs: string) {};
}