import {Diagnosis} from "./diagnosis.model";
import {Performance} from "./performance.model";

export class DiagnosisPerformance {
    constructor(
        public diagnosis: Diagnosis,
        public performance: Performance,
        public localization: string,
        public amount: number
    ) {};
}