import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';

import { CompleterData, CompleterItem } from 'ng2-completer';


export class PetientAutoCompleteService extends Subject<CompleterItem[]> implements CompleterData {
    constructor(private http: Http) {
        super();
    }

    public search(term: string): void {
        this.http.get('/api/patients?first=0&max=10&search=' + term)
            .map((res: Response) => {
                // Convert the result to CompleterItem[]
                let data = res.json();
                let matches: CompleterItem[] = data.map((patient: any) => {
                    return {
                        title: patient.firstname + ' ' + patient.lastname,
                        description: 'nar.: ' +  patient.birthdate,
                        image: patient.photo,
                        originalObject: patient
                    }
                });
                this.next(matches);
            })
            .subscribe();
    }

    public cancel() {
        // Handle cancel
    }
}