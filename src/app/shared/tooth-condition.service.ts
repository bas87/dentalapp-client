import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './authentication.service';
import {Tooth} from "./tooth.model";
import {ToothCondition} from "./tooth-condition.model";


@Injectable()
export class ToothConditionService {

    private requestOptions: RequestOptions;
    private toothConditions: ToothCondition[];

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {

            // add authorization header with jwt token
            let headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + authenticationService.token
            });
           this.requestOptions = new RequestOptions({ headers: headers });
    }

    getToothConditions(): Observable<ToothCondition[]> {
        if (this.toothConditions) {
            return Observable.of(this.toothConditions);
        } else {
            return this.http.get('/api/teeth/conditions', this.requestOptions)
                .map(this.extractData)
                .do(toothConditions => { this.toothConditions = toothConditions; });
        }
    }

    private extractData(res: Response): ToothCondition[] {
        let body = res.json();
        return body.map(r => <ToothCondition>({
            id: r.id,
            name: r.name
        }));
    }
}