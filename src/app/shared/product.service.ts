import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './authentication.service';
import {Product} from "./product.model";


@Injectable()
export class ProductService {

    private requestOptions: RequestOptions;
    private products: Product[];

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {

            // add authorization header with jwt token
            let headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + authenticationService.token
            });
           this.requestOptions = new RequestOptions({ headers: headers });
    }

    getProducts(): Observable<Product[]> {
        if (this.products) {
            return Observable.of(this.products);
        } else {
            return this.http.get('/api/products', this.requestOptions)
                .map(this.extractData)
                .do(products => { this.products = products; });
        }
    }

    private extractData(res: Response): Product[] {
        let body = res.json();
        return body.map(r => <Product>({
            id: r.id,
            name: r.name,
            code: r.code
        }));
    }
}