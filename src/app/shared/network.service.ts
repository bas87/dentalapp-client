import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './authentication.service';
import { CalendarEvent } from './calendar-event.model';
import {Patient} from "./patient.model";
import {PatientService} from "./patient.service";


@Injectable()
export class NetworkService {

    online$: Observable<boolean>;

    constructor() {
        this.online$ = Observable.merge(
            Observable.of(navigator.onLine),
            Observable.fromEvent(window, 'online').map(() => true),
            Observable.fromEvent(window, 'offline').map(() => false)
        );
    }

}