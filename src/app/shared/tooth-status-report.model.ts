import {Tooth} from "./tooth.model";
import {ToothCondition} from "./tooth-condition.model";

export class ToothStatusReport {
    constructor(
        public tooth: Tooth,
        public toothCondition: ToothCondition
    ) {};
}