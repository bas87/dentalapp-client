import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './authentication.service';
import {InsuranceCompany} from "./insurance-company.model";


@Injectable()
export class InsuranceCompanyService {

    private requestOptions: RequestOptions;
    private insuranceCompanies: InsuranceCompany[];

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {

            // add authorization header with jwt token
            let headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + authenticationService.token
            });
           this.requestOptions = new RequestOptions({ headers: headers });
    }

    getInsuranceCompanies(): Observable<InsuranceCompany[]> {
        if (this.insuranceCompanies) {
            return Observable.of(this.insuranceCompanies);
        } else {
            return this.http.get('/api/insurance/companies', this.requestOptions)
                .map(this.extractData)
                .do(insuranceCompanies => { this.insuranceCompanies = insuranceCompanies;  });
        }
    }

    private extractData(res: Response): InsuranceCompany[] {
        let body = res.json();
        return body.map(r => <InsuranceCompany>({
            id: r.id,
            name: r.name,
            code: r.code
        }));
    }
}