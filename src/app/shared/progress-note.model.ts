import {User} from "./user.model";
import {Patient} from "./patient.model";
import {DiagnosisPerformance} from "./diagnosis-performance.model";
import {DiagnosisProduct} from "./diagnosis-product.model";
import {ToothStatusReport} from "./tooth-status-report.model";

export class ProgressNote {
    constructor(
        public id: number,
        public user: User,
        public patient: Patient,
        public created: string,
        public diagnosisPerformances: DiagnosisPerformance[],
        public diagnosisProducts: DiagnosisProduct[],
        public teethStatusReport: ToothStatusReport[],
        public text: string,
    ) {};
}