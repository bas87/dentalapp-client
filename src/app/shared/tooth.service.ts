import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './authentication.service';
import {Tooth} from "./tooth.model";


@Injectable()
export class ToothService {

    private requestOptions: RequestOptions;
    private teeth: Tooth[];

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {

            // add authorization header with jwt token
            let headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + authenticationService.token
            });
           this.requestOptions = new RequestOptions({ headers: headers });
    }

    getTeeth(): Observable<Tooth[]> {
        if (this.teeth) {
            return Observable.of(this.teeth);
        } else {
            return this.http.get('/api/teeth', this.requestOptions)
                .map(this.extractData)
                .do(teeth => { this.teeth = teeth; });
        }
    }

    getTooth(toothId): Observable<Tooth> {
        return this.getTeeth().map(teeth => {
            for (let tooth of teeth) {
                if(tooth.id == toothId) {
                    return tooth;
                }
            }

            return null;
        });
    }

    private extractData(res: Response): Tooth[] {
        let body = res.json();
        return body.map(r => <Tooth>({
            id: r.id,
            name: r.name
        }));
    }
}