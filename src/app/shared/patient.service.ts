import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import {Observable, Subject} from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './authentication.service';
import { Patient } from './patient.model';
import { InsuranceCompanyService } from './insurance-company.service';
import { InsuranceTypeService } from './insurance-type.service';
import { InsuranceCompany } from "./insurance-company.model";
import { InsuranceType } from "./insurance-type.model";


@Injectable()
export class PatientService {

    private requestOptions: RequestOptions;

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService,
        private insuranceCompanyService: InsuranceCompanyService,
        private insuranceTypeService: InsuranceTypeService) {

            // add authorization header with jwt token
            let headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + authenticationService.token
            });
           this.requestOptions = new RequestOptions({ headers: headers });
    }


    getTodaysPatients(fitst: number, max: number): Observable<Patient[]> {

        let today = new Date().toISOString().slice(0,10);

        return Observable.forkJoin([
            this.insuranceCompanyService.getInsuranceCompanies(),
            this.insuranceTypeService.getInsuranceTypes(),
            this.http.get('/api/patients?hasEventFrom=' + today + '&hasEventTo=' + today + 'first=' + fitst + '&max=' + max).map(res => res.json()),
        ]).map((data: any[]) => {
                let insuranceCompanies: InsuranceCompany[] = data[0];
                let insuranceTypes: InsuranceType[] = data[1];
                let patients: any[] = data[2];

                return patients.map(r => {
                    return this.extractData(r, insuranceTypes, insuranceCompanies);
                });

            });
    }

    getPatients(fitst: number, max: number): Observable<Patient[]> {

        return Observable.forkJoin([
            this.insuranceCompanyService.getInsuranceCompanies(),
            this.insuranceTypeService.getInsuranceTypes(),
            this.http.get('/api/patients?first=' + fitst + '&max=' + max).map(res => res.json()),
        ]).map((data: any[]) => {
            let insuranceCompanies: InsuranceCompany[] = data[0];
            let insuranceTypes: InsuranceType[] = data[1];
            let patients: any[] = data[2];

            return patients.map(r => {
                return this.extractData(r, insuranceTypes, insuranceCompanies);
            });

        });
    }

    getPatient(id: number): Observable<Patient> {

        return Observable.forkJoin([
            this.insuranceCompanyService.getInsuranceCompanies(),
            this.insuranceTypeService.getInsuranceTypes(),
            this.http.get('/api/patients/' + id).map(res => res.json()),
        ]).map((data: any[]) => {
            let insuranceCompanies: InsuranceCompany[] = data[0];
            let insuranceTypes: InsuranceType[] = data[1];
            let patient: any = data[2];

            return this.extractData(patient, insuranceTypes, insuranceCompanies);
        });
    }

    createPatient(patient: Patient): Observable<Patient>  {

        let patientData = this.prepareData(patient);

        return this.http.post('/api/patients', patientData, this.requestOptions)
            .map((res:Response) => res.json());

    }

   updatePatient(patient: Patient): Observable<Patient>  {

       let patientData = this.prepareData(patient);

        return this.http.put('/api/patients/' + patient.id, patientData, this.requestOptions)
            .map((res:Response) => res.json());

    }

    private extractData(r: any,
        insuranceTypes: InsuranceType[],
        insuranceCompanies: InsuranceCompany[]): Patient {

        return <Patient>({
            id: r.id,
            insuranceType: this.findInsuranceType(insuranceTypes, r.insuranceTypeId),
            insuranceCompany: this.findInsuranceCompany(insuranceCompanies, r.insuranceCompanyId),
            firstname: r.firstname,
            lastname: r.lastname,
            photo: r.photo,
            birthdate: r.birthdate,
            rc: r.rc,
            sex: r.sex,
            phoneNumber: r.phoneNumber,
            email: r.email,
            street: r.street,
            city: r.city,
            zip: r.zip,
            insurenceNumber: r.insurenceNumber,
            medicalHistory: r.medicalHistory,
            allergy: r.allergy,
            drugs: r.drugs
        });

    }

    private prepareData(r: Patient): any {

        let data = {
            firstname: r.firstname,
            lastname: r.lastname,
            birthdate: r.birthdate,
            photo: r.photo,
            rc: r.rc,
            sex: r.sex,
            phoneNumber: r.phoneNumber,
            email: r.email,
            street: r.street,
            city: r.city,
            zip: r.zip,
            insurenceNumber: r.insurenceNumber,
            medicalHistory: r.medicalHistory,
            allergy: r.allergy,
            drugs: r.drugs
        };

        if(r.hasOwnProperty('id') && r.id != null) {
            data['id'] = r.id;
        }

        if(r.hasOwnProperty('insuranceType') && r.insuranceType != null) {
            data['insuranceTypeId'] = r.insuranceType.id;
        }

        if(r.hasOwnProperty('insuranceCompany') && r.insuranceCompany != null) {
            console.log(r.insuranceCompany );
            data['insuranceCompanyId'] = r.insuranceCompany.id;
        }

        return data;

    }

    private findInsuranceCompany(insuranceCompanies: InsuranceCompany[], id: number): InsuranceCompany {
        let insuranceCompany =  insuranceCompanies.filter(company => { return company.id == id; });

        if (insuranceCompany) {
            return insuranceCompany[0];
        }

        return null;
    }

    private findInsuranceType(insuranceTypes: InsuranceType[], id: number): InsuranceType {
        let insuranceType =  insuranceTypes.filter(type => { return type.id == id; });

        if (insuranceType) {
            return insuranceType[0];
        }

        return null;
    }

}