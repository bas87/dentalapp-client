export class InsuranceType {
    constructor(
        public id: number,
        public name: string) {};
}