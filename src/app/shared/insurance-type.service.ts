import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './authentication.service';
import {InsuranceType} from "./insurance-type.model";


@Injectable()
export class InsuranceTypeService {

    private requestOptions: RequestOptions;
    private insuranceTypes: InsuranceType[];

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {

            // add authorization header with jwt token
            let headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + authenticationService.token
            });
           this.requestOptions = new RequestOptions({ headers: headers });
    }

    getInsuranceTypes(): Observable<InsuranceType[]> {
        if (this.insuranceTypes) {
            return Observable.of(this.insuranceTypes);
        } else {
            return this.http.get('/api/insurance/types', this.requestOptions)
                .map(this.extractData)
                .do(insuranceTypes => { this.insuranceTypes = insuranceTypes; });
        }
    }

    private extractData(res: Response): InsuranceType[] {
        let body = res.json();
        return body.map(r => <InsuranceType>({
            id: r.id,
            name: r.name
        }));
    }
}