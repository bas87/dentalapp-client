import { Patient } from './patient.model';

export class CalendarEvent {
    constructor(
        public id: number,
        public start: string,
        public end: string,
        public title: string,
        public patient: Patient) {};
}