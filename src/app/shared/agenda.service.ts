import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './authentication.service';
import { CalendarEvent } from './calendar-event.model';
import {Patient} from "./patient.model";
import {PatientService} from "./patient.service";


@Injectable()
export class AgendaService {

    private requestOptions: RequestOptions;

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService,
        private patientService: PatientService) {

            // add authorization header with jwt token
            let headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + authenticationService.token
            });
           this.requestOptions = new RequestOptions({ headers: headers });
    }

    getEvents(): Observable<CalendarEvent[]> {
        return this.http.get('/api/events', this.requestOptions)
            .map(this.extractData);
    }

    getTodaysEvents(): Observable<CalendarEvent[]> {
        let today = new Date().toISOString().slice(0,10);

        return this.http.get('/api/events?from=' + today + '&to=' + today, this.requestOptions)
            .map(this.extractData)
            .flatMap((events: CalendarEvent[]) => {
                if (events.length > 0) {
                    return Observable.forkJoin(
                        events.map((event: CalendarEvent) => {
                            return this.patientService.getPatient(event.patient.id)
                                .map((patient: Patient) => {
                                    event.patient = patient;
                                    return event;
                                });
                        })
                    );
                }
                return Observable.of([]);
            });
    }

    createEvent(event: CalendarEvent): Observable<CalendarEvent>  {
        let eventData = this.prepareData(event);

        return this.http.post('/api/events', eventData, this.requestOptions)
            .map((res:Response) => res.json());

    }

    private extractData(res: Response): CalendarEvent[] {
        let body = res.json();
        return body.map(r => <CalendarEvent>({
            id: r.id,
            start: r.start,
            end: r.end,
            title: r.title,
            patient: <Patient>({ id: r.patientId })
        }));
    }

    private prepareData(r: CalendarEvent): any {

        let data = {
            start: r.start,
            end: r.end,
            title: r.title,
            patientId: r.patient.id
        };

        if(r.hasOwnProperty('id')) {
            data['id'] = r.id;
        }

        return data;

    }
}