import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './authentication.service';
import {Diagnosis} from "./diagnosis.model";


@Injectable()
export class DiagnosisService {

    private requestOptions: RequestOptions;
    private diagnoses: Diagnosis[];

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {

            // add authorization header with jwt token
            let headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + authenticationService.token
            });
           this.requestOptions = new RequestOptions({ headers: headers });
    }

    getDiagnoses(): Observable<Diagnosis[]> {
        if (this.diagnoses) {
            return Observable.of(this.diagnoses);
        } else {
            return this.http.get('/api/diagnoses', this.requestOptions)
                .map(this.extractData)
                .do(diagnoses => { this.diagnoses = diagnoses; });
        }
    }

    private extractData(res: Response): Diagnosis[] {
        let body = res.json();
        return body.map(r => <Diagnosis>({
            id: r.id,
            name: r.name,
            code: r.code
        }));
    }
}