import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './authentication.service';
import {Performance} from "./performance.model";


@Injectable()
export class PerformanceService {

    private requestOptions: RequestOptions;
    private performances: Performance[];

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {

            // add authorization header with jwt token
            let headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + authenticationService.token
            });
           this.requestOptions = new RequestOptions({ headers: headers });
    }

    getPerformances(): Observable<Performance[]> {
        if (this.performances) {
            return Observable.of(this.performances);
        } else {
            return this.http.get('/api/performances', this.requestOptions)
                .map(this.extractData)
                .do(performances => { this.performances = performances; });
        }
    }

    private extractData(res: Response): Performance[] {
        let body = res.json();
        return body.map(r => <Performance>({
            id: r.id,
            name: r.name,
            code: r.code
        }));
    }
}