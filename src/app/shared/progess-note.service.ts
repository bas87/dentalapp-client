import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './authentication.service';
import { Patient } from './patient.model';
import {ProgressNote} from "./progress-note.model";
import {DiagnosisService} from "./diagnosis.service";
import {ProductService} from "./product.service";
import {PerformanceService} from "./performance.service";
import {User} from "./user.model";
import {Diagnosis} from "./diagnosis.model";
import {Product} from "./product.model";
import {Performance} from "./performance.model";
import {DiagnosisPerformance} from "./diagnosis-performance.model";
import {DiagnosisProduct} from "./diagnosis-product.model";
import {ToothService} from "./tooth.service";
import {ToothConditionService} from "./tooth-condition.service";
import {Tooth} from "./tooth.model";
import {ToothCondition} from "./tooth-condition.model";
import {ToothStatusReport} from "./tooth-status-report.model";


@Injectable()
export class ProgressNoteService {

    private requestOptions: RequestOptions;

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService,
        private dianosisService: DiagnosisService,
        private productService: ProductService,
        private performanceService: PerformanceService,
        private toothService: ToothService,
        private toothConditionService: ToothConditionService) {

            // add authorization header with jwt token
            let headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + authenticationService.token
            });
           this.requestOptions = new RequestOptions({ headers: headers });
    }

    getProgressNotes(patientId:number, fitst: number, max: number): Observable<ProgressNote[]> {

        return Observable.forkJoin([
            this.dianosisService.getDiagnoses(),
            this.productService.getProducts(),
            this.performanceService.getPerformances(),
            this.toothService.getTeeth(),
            this.toothConditionService.getToothConditions(),
            this.http.get('/api/patients/' +  patientId + '/progress-notes?first=' + fitst + '&max=' + max).map(res => res.json()),
        ]).map((data: any[]) => {
            let diagnoses: Diagnosis[] = data[0];
            let products: Product[] = data[1];
            let performances: Performance[] = data[2];
            let teeth: Tooth[] = data[3];
            let toothConditions: ToothCondition[] = data[4];
            let progressNotes: any[] = data[5];

            return progressNotes.map(r => {
                return this.extractData(r, diagnoses, products, performances, teeth, toothConditions);
            });

        });
    }

    createProgressNote(progressNote: ProgressNote): Observable<ProgressNote>  {

        let progressNoteData = this.prepareData(progressNote);

        return this.http.post('/api/progress-notes', progressNoteData, this.requestOptions)
            .map((res:Response) => res.json());

    }

    updateProgressNote(progressNote: ProgressNote): Observable<ProgressNote>  {

        let progressNoteData = this.prepareData(progressNote);

        return this.http.put('/api/progress-notes/'+progressNote.id, progressNoteData, this.requestOptions)
            .map((res:Response) => res.json());

    }

    private prepareData(r: ProgressNote): any {

        let data = {
            userId: r.user.id,
            patientId: r.patient.id,
            created: r.created,
            text: r.text
        };

        if(r.hasOwnProperty('id') && r.id != null) {
            data['id'] = r.id;
        }

        if(r.hasOwnProperty('diagnosisPerformances') && r.diagnosisPerformances != null) {
            data['performances'] = r.diagnosisPerformances.map(diagnosisPerformance => {
                return {
                    diagnosisId: diagnosisPerformance.diagnosis.id,
                    performanceId: diagnosisPerformance.performance.id,
                    localization: diagnosisPerformance.localization,
                    amount: diagnosisPerformance.amount,
                };
            });
        }

        if(r.hasOwnProperty('diagnosisProducts') && r.diagnosisProducts != null) {
            data['products'] = r.diagnosisProducts.map(diagnosisProduct => {
                return {
                    diagnosisId: diagnosisProduct.diagnosis.id,
                    productId: diagnosisProduct.product.id,
                    localization: diagnosisProduct.localization,
                    amount: diagnosisProduct.amount,
                };
            });
        }

        if(r.hasOwnProperty('teethStatusReport') && r.teethStatusReport != null) {
            data['teethStatusReport'] = r.teethStatusReport.filter(toothStatusReport => {
                return toothStatusReport.toothCondition;
            }).map(toothStatusReport => {
                return {
                    toothId: toothStatusReport.tooth.id,
                    toothConditionId: toothStatusReport.toothCondition.id,
                };
            });
        }

        return data;

    }

    private extractData(r: any,
        dianoses: Diagnosis[],
        products: Product[],
        performances: Performance[],
        teeth: Tooth[],
        toothConditions: ToothCondition[]): ProgressNote {

        return <ProgressNote>({
            id: r.id,
            user: <User>{},
            patient: <Patient>{},
            created: r.created,
            diagnosisPerformances: r.performances.map(diagnosisPerformance => {
                return <DiagnosisPerformance>({
                    diagnosis: this.findDiagnosis(dianoses, diagnosisPerformance.diagnosisId),
                    performance: this.findPerformance(performances, diagnosisPerformance.performanceId),
                    localization: diagnosisPerformance.localization,
                    amount: diagnosisPerformance.amount,
                });
            }),
            diagnosisProducts: r.products.map(diagnosisProduct => {
                return <DiagnosisProduct>({
                    diagnosis: this.findDiagnosis(dianoses, diagnosisProduct.diagnosisId),
                    product: this.findProduct(products, diagnosisProduct.productId),
                    localization: diagnosisProduct.localization,
                    amount: diagnosisProduct.amount,
                });
            }),
            teethStatusReport: r.teethStatusReport.map(teethStatusReport => {
                return <ToothStatusReport>({
                    tooth: this.findTooth(teeth, teethStatusReport.toothId),
                    toothCondition: this.findToothCondition(toothConditions, teethStatusReport.toothConditionId)
                });
            }),
            text: r.text
        });
    }

    private findDiagnosis(dianoses: Diagnosis[], id: number): Diagnosis {
        let diagnosis =  dianoses.filter(diagnosis => { return diagnosis.id == id; });

        if (diagnosis) {
            return diagnosis[0];
        }

        return null;
    }

    private findProduct(products: Product[], id: number): Product {
        let product =  products.filter(product => { return product.id == id; });

        if (product) {
            return product[0];
        }

        return null;
    }

    private findPerformance(performances: Performance[], id: number): Performance {
        let performance =  performances.filter(performance => { return performance.id == id; });

        if (performance) {
            return performance[0];
        }

        return null;
    }

    private findTooth(teeth: Tooth[], id: number): Tooth {
        let tooth =  teeth.filter(tooth => { return tooth.id == id; });

        if (tooth) {
            return tooth[0];
        }

        return null;
    }

    private findToothCondition(toothConditions: ToothCondition[], id: number): Tooth {
        let toothCondition =  toothConditions.filter(toothCondition => { return toothCondition.id == id; });

        if (toothCondition) {
            return toothCondition[0];
        }

        return null;
    }

}