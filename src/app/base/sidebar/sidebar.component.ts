import { Component } from '@angular/core';


@Component({
    selector: 'da-sidebar',
    styleUrls: [
        './sidebar.component.scss'
    ],
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent {

    hover = true;

    onClose() {
        this.hover = false;
    }
}