import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from '../shared/auth-guard.service';
import { ViewportService } from './shared/viewport.service';
import { routedComponents, BaseRoutingModule } from './base-routing.module';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { Ng2CompleterModule } from "ng2-completer";
import { HeaderComponent } from './header';
import { SidebarComponent } from './sidebar';
import { ToasterComponent } from "./toaster/toaster.component";
import { ToastComponent } from "./toaster/toast.component";
import { ToasterService } from "./toaster/toaster.service";
import {NetworkService} from "../shared/network.service";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        BaseRoutingModule,
        InfiniteScrollModule,
        Ng2CompleterModule
    ],
    declarations: [
        routedComponents,
        HeaderComponent,
        SidebarComponent,
        ToasterComponent,
        ToastComponent
    ],
    providers: [
        AuthGuard,
        ViewportService,
        ToasterService,
        NetworkService
    ]
})
export default class BaseModule { }