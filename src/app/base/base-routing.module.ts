import { NgModule }       from '@angular/core';
import { RouterModule }   from '@angular/router';
import { BaseComponent } from './base.component';
import { AuthGuard } from '../shared/auth-guard.service';

export const routedComponents = [BaseComponent];


@NgModule({
    imports: [
        RouterModule.forChild([{
            path: '',
            component: BaseComponent,
            canActivate: [AuthGuard],
            children: [
                { path: 'patients', loadChildren: './patients' },
                { path: 'calendar', loadChildren: './calendar' },
                { path: '', loadChildren: './home'}
            ]
        }])
    ],
    exports: [RouterModule]
})
export class BaseRoutingModule { }