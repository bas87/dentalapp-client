export class Toast {
  constructor(
      public id: number,
      public msg: string,
      public timeout: number) {};
}