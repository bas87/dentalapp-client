import { Component, OnInit } from '@angular/core';

import { Toast } from './toast.model';
import { ToasterService } from './toaster.service';

@Component ({
    selector: 'da-toaster',
    template: `
        <div class="toaster">
            <da-toast *ngFor="let toast of toasts" [toast]="toast"></da-toast>
        </div>`,
    styleUrls: [
        './toaster.component.scss'
    ]
})
export class ToasterComponent implements OnInit {
    
    toasts: Array<Toast>;

    constructor(private _toasterService: ToasterService) {
        this.toasts  = [];
    }

    ngOnInit() {
        
        this._toasterService.push$.subscribe(
            (toast: Toast) => {
                if (toast != undefined) {
                    this.toasts.push(toast);
                    if (toast.timeout) {
                        this._setTimeout(toast);
                    }
                }
            }
        );


        this._toasterService.pull$.subscribe(
            (toastId: number) => {
                if (toastId) {
                    this.clear(toastId);
                }
            }
        );

    }

    private _setTimeout(toast: Toast) {
        if(toast.timeout > 0) {
            window.setTimeout(() => {
                this.clear(toast.id);
            }, toast.timeout);
        }    
    }

    clear(id: number) {
        if (id) {
            this.toasts.forEach((value: any, key: number) => {
                if (value.id === id) {
                    if (value.onRemove && this.isFunction(value.onRemove)) {
                        value.onRemove.call(this, value);
                    }
                    this.toasts.splice(key, 1);
                }
            });
        } 
        else {
            throw new Error('Please provide id of Toast to close');
        }
    }

    private isFunction(obj: any) {
        return typeof obj === "function";
    }

}
