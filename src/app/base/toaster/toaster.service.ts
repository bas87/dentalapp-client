import { Injectable } from '@angular/core';

import { Toast } from './toast.model';
import {Subject} from "rxjs";

@Injectable ()
export class ToasterService {

    // counter
    uniqueCounter: number = 0;

    // Observable Toasts sources
    private pushedToastsEmitter = new Subject<Toast>();
    private pulledToastsEmitter = new Subject<number>();

    // Observable Toasts streams
    push$ = this.pushedToastsEmitter.asObservable();
    pull$ = this.pulledToastsEmitter.asObservable();

    push(msg: string, timeout: number): Toast {
        this.uniqueCounter++;
        let toast: Toast = new Toast(this.uniqueCounter, msg, timeout);

        this.pushedToastsEmitter.next(toast);

        return toast;
    }

    pull(toastId: number): void {
        this.pulledToastsEmitter.next(toastId);
    }

}
