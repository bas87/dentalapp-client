import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Toast } from './toast.model';

@Component ({
    selector: 'da-toast',
    template: `<span class="toast-msg">{{toast.msg}}</span>`,
    styleUrls: [
        './toast.component.scss'
    ],
})
export class ToastComponent implements OnChanges {

    @Input() toast: Toast;

    ngOnChanges() {
    }
}