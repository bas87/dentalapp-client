import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class ViewportService {

    // Observable number sources
    private scrollDownCounter = new Subject<number>();
    private scrollUpCounter = new Subject<number>();

    // Observable number streams
    scrolledDown$ = this.scrollDownCounter.asObservable();
    scrolledUp$ = this.scrollUpCounter.asObservable();

    // Service message commands
    logScrollDown() {
        this.scrollDownCounter.next(1);
    }

    logScrollUp() {
        this.scrollUpCounter.next(1);
    }
}