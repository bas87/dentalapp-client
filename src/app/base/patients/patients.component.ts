import { Component, OnInit, OnDestroy } from '@angular/core';
import { Patient } from '../../shared/patient.model';
import { PatientService } from '../../shared/patient.service';
import { Subscription }   from 'rxjs/Subscription';
import { ViewportService } from '../shared/viewport.service';


@Component({
  selector: 'patients',
  styleUrls: [
    './patients.component.scss'
  ],
  templateUrl: './patients.component.html'
})
export class PatientsComponent implements OnInit, OnDestroy {
    patients: Patient[] = [];
    loading = false;
    subscription: Subscription;
    sum = 20;

    constructor(private patientService: PatientService,
        private viewportService: ViewportService) {

        this.subscription = viewportService.scrolledDown$.subscribe(
            viewport => {

                this.patientService.getPatients(this.sum, 20)
                    .subscribe(patients => {
                        console.info('from scroll event');

                        this.patients.push.apply(this.patients, patients)

                        this.loading = false;

                        this.sum += 20;

                    });
            });
    }

    ngOnInit() {
        // get users from secure api end point
        this.loading = true;
        this.patientService.getPatients(0, 20)
            .subscribe(patients => {

                console.info('from init event');

                this.patients = patients;
                this.loading = false;
        });
    }

    ngOnDestroy() {
        // prevent memory leak when component destroyed
        this.subscription.unsubscribe();
    }
}