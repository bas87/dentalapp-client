import {Component, OnInit, ViewChild} from '@angular/core';
import * as jQeury from 'jquery';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ProgressNoteService} from "../../../shared/progess-note.service";
import {ProgressNote} from "../../../shared/progress-note.model";
import {Observable} from "rxjs";
import {DiagnosisService} from "../../../shared/diagnosis.service";
import {ProductService} from "../../../shared/product.service";
import {PerformanceService} from "../../../shared/performance.service";
import {Diagnosis} from "../../../shared/diagnosis.model";
import {Product} from "../../../shared/product.model";
import {Performance} from "../../../shared/performance.model";
import {DiagnosisPerformance} from "../../../shared/diagnosis-performance.model";
import {DiagnosisProduct} from "../../../shared/diagnosis-product.model";
import {User} from "../../../shared/user.model";
import {DatePipe} from "@angular/common";
import {Patient} from "../../../shared/patient.model";
import {ToasterService} from "../../toaster/toaster.service";

@Component({
  selector: 'da-progress-notes',
  styleUrls: [
    './progress-notes.component.scss'
  ],
  templateUrl: './progress-notes.component.html'
})
export class ProgressNotesComponent  implements OnInit {

    loading = false;
    rootRoute: string;

    progressNotes: ProgressNote[];
    progressNote: ProgressNote = <ProgressNote>({
        text: <string>'',
        diagnosisPerformances: <DiagnosisPerformance[]>[],
        diagnosisProducts: <DiagnosisProduct[]>[]
    });
    diagnosisPerformance: DiagnosisPerformance = <DiagnosisPerformance>{};
    diagnosisProduct: DiagnosisProduct = <DiagnosisProduct>{};

    diagnoses: Diagnosis[];
    products: Product[];
    performances: Performance[];
    patient: Patient;

    @ViewChild('createNewProgressNoteModal') createNewProgressNoteModal;

    constructor(
        private route: ActivatedRoute,
        private progressNoteService: ProgressNoteService,
        private diagnosisService: DiagnosisService,
        private productService: ProductService,
        private performanceService: PerformanceService,
        private datePipe: DatePipe,
        private toaster: ToasterService
    ) { }

    onSave() {
        jQeury(this.createNewProgressNoteModal.nativeElement).modal('hide');

        this.progressNote.user = new User(1, 'admin', null, 'Michal', 'Toman', null);
        this.progressNote.created =  this.datePipe.transform(new Date, 'y-MM-dd');
        this.progressNote.patient = this.patient;


        this.progressNoteService.createProgressNote(this.progressNote).subscribe(data => {
            this.ngOnInit();//chro chro
            this.toaster.push('Záznam dekurzu byl uložen.',2000);
        });
    }

    onSavePerformance() {
        let clone = <DiagnosisPerformance>JSON.parse(JSON.stringify(this.diagnosisPerformance)); // deep clone
        this.progressNote.diagnosisPerformances.push(clone);

        this.progressNote.text = this.progressNote.text + clone.diagnosis.name + '\n';
    }

    onSaveProduct() {
        let clone = <DiagnosisProduct>JSON.parse(JSON.stringify(this.diagnosisProduct)); // deep clone
        this.progressNote.diagnosisProducts.push(clone);

        this.progressNote.text = this.progressNote.text + clone.diagnosis.name + '\n';
    }

    ngOnInit() {
        this.loading = true;

        this.route.params
            .subscribe((params: Params) => {
                this.rootRoute = '/patients/' + params['id'];
                this.patient = <Patient>{id: params['id']};

                return Observable.forkJoin([
                    this.diagnosisService.getDiagnoses(),
                    this.productService.getProducts(),
                    this.performanceService.getPerformances(),
                    this.progressNoteService.getProgressNotes(params['id'], 0, 100),
                ]).subscribe((data: any[]) => {

                    this.diagnoses = <Diagnosis[]>data[0];
                    this.products = <Product[]>data[1];
                    this.performances = <Performance[]>data[2];
                    this.progressNotes = <ProgressNote[]>data[3];

                    this.loading = false;
                });

            });
    }

}