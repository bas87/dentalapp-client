import { Component, OnInit, ViewChild } from '@angular/core';
import { PatientService } from '../../../shared/patient.service';
import { Patient } from "../../../shared/patient.model";
import { Observable } from 'rxjs';
import { InsuranceCompanyService } from '../../../shared/insurance-company.service';
import { InsuranceTypeService } from '../../../shared/insurance-type.service';
import {InsuranceCompany} from "../../../shared/insurance-company.model";
import {InsuranceType} from "../../../shared/insurance-type.model";
import {Router} from "@angular/router";
import {ToasterService} from "../../toaster/toaster.service";

@Component({
  selector: 'da-new-patient',
  styleUrls: [
    './new-patient.component.scss'
  ],
  templateUrl: './new-patient.component.html'
})
export class NewPatientComponent  implements OnInit {

    patient: Patient = <Patient>{};
    loading = false;
    rootRoute: string;
    insuranceCompanies: InsuranceCompany[];
    insuranceTypes: InsuranceType[];

    @ViewChild('calendarEventModal') calendarEventModal;

    constructor(
        private router: Router,
        private patientService: PatientService,
        private insuranceCompanyService: InsuranceCompanyService,
        private insuranceTypeService: InsuranceTypeService,
        private toaster: ToasterService
    ) { }

    onSave() {
        this.patientService.createPatient(<Patient>this.patient).subscribe(data => {
            this.toaster.push('Pacient byl uložen.',2000);
            this.router.navigate(['/patients', data.id]);
        });
    }

    ngOnInit() {
        this.loading = true;

        Observable.forkJoin([
            this.insuranceCompanyService.getInsuranceCompanies(),
            this.insuranceTypeService.getInsuranceTypes(),
        ]).subscribe((data: any[]) => {

            this.insuranceCompanies = data[0];
            this.insuranceTypes = data[1];

            this.loading = false;
        });

    }

}