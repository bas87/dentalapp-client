import { Component, OnInit, ViewChild } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as jQeury from 'jquery';
import { PatientService } from '../../../shared/patient.service';
import { Patient } from "../../../shared/patient.model";
import { Observable } from 'rxjs';
import { InsuranceCompanyService } from '../../../shared/insurance-company.service';
import { InsuranceTypeService } from '../../../shared/insurance-type.service';
import {InsuranceCompany} from "../../../shared/insurance-company.model";
import {InsuranceType} from "../../../shared/insurance-type.model";
import {CalendarEvent} from "../../../shared/calendar-event.model";
import {AgendaService} from "../../../shared/agenda.service";
import {DatePipe} from "@angular/common";
import {ToasterService} from "../../toaster/toaster.service";

@Component({
  selector: 'da-patient',
  styleUrls: [
    './patient.component.scss'
  ],
  templateUrl: './patient.component.html'
})
export class PatientComponent  implements OnInit {

    patient: Patient = <Patient>({});
    calendarEvent: CalendarEvent = <CalendarEvent>({});
    loading = false;
    rootRoute: string;
    insuranceCompanies: InsuranceCompany[];
    insuranceTypes: InsuranceType[];

    @ViewChild('calendarEventModal') calendarEventModal;

    constructor(
        private route: ActivatedRoute,
        private patientService: PatientService,
        private insuranceCompanyService: InsuranceCompanyService,
        private insuranceTypeService: InsuranceTypeService,
        private agendaService: AgendaService,
        private datePipe: DatePipe,
        private toaster: ToasterService
    ) { }

    onSave() {
        this.patientService.updatePatient(<Patient>this.patient).subscribe(data => {
            this.toaster.push('Pacient byl uložen.',2000);
        });
    }

    onSaveCalendarEvent() {

        let start = new Date(this.calendarEvent.start.replace('T',' '));
        let end = new Date(start.getTime() + (+this.calendarEvent.end) *60000);

        this.calendarEvent.start = this.datePipe.transform(start, 'y-MM-dd HH:mm:ss');
        this.calendarEvent.end = this.datePipe.transform(end, 'y-MM-dd HH:mm:ss');
        this.calendarEvent.patient = this.patient;

        this.agendaService.createEvent(this.calendarEvent).subscribe(data => {
            jQeury(this.calendarEventModal.nativeElement).modal('hide');
            this.toaster.push('Návštěva byla uložena do kalendáře.',2000);
        });
    }

    ngOnInit() {
        this.loading = true;

        this.route.params
            .subscribe(params => {

                Observable.forkJoin([
                    this.patientService.getPatient(+params['id']),
                    this.insuranceCompanyService.getInsuranceCompanies(),
                    this.insuranceTypeService.getInsuranceTypes(),
                ]).subscribe((data: any[]) => {

                    this.insuranceCompanies = data[1];
                    this.insuranceTypes = data[2];

                    //Fix -- maybe bug in compiler
                    this.patient =  data[0];
                    if (this.patient.insuranceType) {
                        this.patient.insuranceType = this.insuranceTypes.filter(type => { return type.id == this.patient.insuranceType.id; })[0];
                    }
                    if (this.patient.insuranceCompany) {
                        this.patient.insuranceCompany = this.insuranceCompanies.filter(company => { return company.id == this.patient.insuranceCompany.id; })[0];
                    }
                    //--

                    this.calendarEvent.title = this.patient.firstname + ' ' + this.patient.lastname + ' - kontrola';

                    this.loading = false;
                    this.rootRoute = '/patients/' + this.patient.id;
                });

            });

    }

}