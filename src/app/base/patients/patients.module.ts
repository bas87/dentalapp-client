import { NgModule }     from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { FormsModule } from '@angular/forms';
import { routedComponents, PatientsRoutingModule } from './patients-routing.module';
import { PatientService } from '../../shared/patient.service';
import { InsuranceCompanyService } from '../../shared/insurance-company.service';
import { InsuranceTypeService } from '../../shared/insurance-type.service';
import { AuthenticationService } from '../../shared/authentication.service';
import { AuthGuard } from '../../shared/auth-guard.service';
import { PatientComponent } from './patient/patient.component';
import { NewPatientComponent } from './new-patient/new-patient.component';
import { CardComponent } from './shared/card/card.component';
import { ProgressNotesComponent } from './progress-notes/progress-notes.component';
import { TeethStatusReportsComponent } from './teeth-status-reports/teeth-status-reports.component';
import {AgendaService} from "../../shared/agenda.service";
import {ProgressNoteService} from "../../shared/progess-note.service";
import {ProductService} from "../../shared/product.service";
import {DiagnosisService} from "../../shared/diagnosis.service";
import {PerformanceService} from "../../shared/performance.service";
import {ToothService} from "../../shared/tooth.service";
import {ToothConditionService} from "../../shared/tooth-condition.service";

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      PatientsRoutingModule ],
  declarations: [
      routedComponents,
      PatientComponent,
      NewPatientComponent,
      CardComponent,
      ProgressNotesComponent,
      TeethStatusReportsComponent
  ],
  providers: [
        AuthGuard,
        PatientService,
        InsuranceCompanyService,
        InsuranceTypeService,
        AgendaService,
        AuthenticationService,
        DatePipe,
        ProgressNoteService,
        ProductService,
        DiagnosisService,
        PerformanceService,
        ToothService,
        ToothConditionService
  ]
})
export default class PatientsModule { }