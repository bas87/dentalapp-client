import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {ProgressNote} from "../../../shared/progress-note.model";
import {Observable} from "rxjs";
import {ProgressNoteService} from "../../../shared/progess-note.service";
import {ToothService} from "../../../shared/tooth.service";
import {ToothConditionService} from "../../../shared/tooth-condition.service";
import {ToothCondition} from "../../../shared/tooth-condition.model";
import {ToothStatusReport} from "../../../shared/tooth-status-report.model";
import {ToasterService} from "../../toaster/toaster.service";

@Component({
  selector: 'da-teeth-status-reports',
  styleUrls: [
    './teeth-status-reports.component.scss'
  ],
  templateUrl: './teeth-status-reports.component.html'
})
export class TeethStatusReportsComponent  implements OnInit {

    loading = false;
    rootRoute: string;

    progressNotes: ProgressNote[];
    progressNote: ProgressNote;
    toothConditions: ToothCondition[];
    selectedToothStatusReport: ToothStatusReport = <ToothStatusReport>{};

    constructor(
        private route: ActivatedRoute,
        private progressNoteService: ProgressNoteService,
        private toothService: ToothService,
        private toothConditionService: ToothConditionService,
        private toaster: ToasterService
    ) { }

    ngOnInit() {
        this.loading = true;

        this.route.params
            .subscribe((params: Params) => {
                this.rootRoute = '/patients/' + params['id'];

                return Observable.forkJoin([
                    this.progressNoteService.getProgressNotes(params['id'], 0, 100),
                    this.toothConditionService.getToothConditions()
                ]).subscribe((data: any[]) => {

                    this.progressNotes = <ProgressNote[]>data[0];
                    this.toothConditions = <ToothCondition[]>data[1];

                    this.loading = false;
                });

            });
    }

    onClickTooth(toothId:number) {
        this.toothService.getTooth(toothId).subscribe(tooth => {

            if (this.progressNote) {
                if (this.progressNote.teethStatusReport) {
                    let toothStatusReport = this.progressNote.teethStatusReport.filter(toothStatusReport => {
                        return toothStatusReport.tooth.id == toothId;
                    });

                    if (toothStatusReport.length) {

                        this.selectedToothStatusReport = <ToothStatusReport>toothStatusReport[0];

                        for (let tc of this.toothConditions) {
                            if(JSON.stringify(tc) == JSON.stringify(toothStatusReport[0].toothCondition)) {
                                this.selectedToothStatusReport.toothCondition = tc;
                            }
                        }

                    } else {
                        this.selectedToothStatusReport = new ToothStatusReport(tooth, null);
                        this.progressNote.teethStatusReport.push(this.selectedToothStatusReport);
                    }
                }
            }

        });
    }

    isToothSelected(toothId:number): boolean {

        if (this.progressNote) {
            if (this.progressNote.teethStatusReport) {
                let toothStatusReport = this.progressNote.teethStatusReport.filter(toothStatusReport => {
                    return toothStatusReport.tooth.id == toothId;
                });

                if (toothStatusReport.length) {
                    return true;
                }
            }
        }

        return false;
    }

    onSave() {
        if (this.progressNote) {
            this.progressNoteService.updateProgressNote(this.progressNote).subscribe(progressNote => {
                this.ngOnInit();//chro chro
                this.toaster.push('Stav zubu byl uložen.',2000);
            });
        }
    }

}