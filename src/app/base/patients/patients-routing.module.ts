import { NgModule }       from '@angular/core';
import { RouterModule }   from '@angular/router';
import { PatientsComponent } from './patients.component';
import { PatientComponent } from './patient/patient.component';
import { AuthGuard } from '../../shared/auth-guard.service';
import { ProgressNotesComponent } from './progress-notes/progress-notes.component';
import { TeethStatusReportsComponent } from './teeth-status-reports/teeth-status-reports.component';
import {NewPatientComponent} from "./new-patient/new-patient.component";

export const routedComponents = [PatientsComponent, PatientComponent];

@NgModule({
  imports: [
      RouterModule.forChild([
          { path: '', component: PatientsComponent, canActivate: [AuthGuard] },
          { path: 'new', component: NewPatientComponent, canActivate: [AuthGuard] },
          { path: ':id', component: PatientComponent, canActivate: [AuthGuard] },
          { path: ':id/progress-notes', component: ProgressNotesComponent, canActivate: [AuthGuard] },
          { path: ':id/teeth-status-reports', component: TeethStatusReportsComponent, canActivate: [AuthGuard] },
  ])],
  exports: [RouterModule]
})
export class PatientsRoutingModule {}