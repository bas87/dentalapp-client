import {Component, Input } from '@angular/core';

@Component({
  selector: 'da-card',
  styleUrls: [
    'card.component.scss'
  ],
  templateUrl: 'card.component.html'
})
export class CardComponent {
    @Input('rootRoute') rootRoute: string;
    @Input('hideTabs') hideTabs = false;
    @Input() loading = true;
}