import { Component, EventEmitter, Output } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { CompleterItem } from 'ng2-completer';
import { PetientAutoCompleteService } from '../../shared/patient-autocomplete.service';


@Component({
    selector: 'da-header',
    styleUrls: [
        './header.component.scss'
    ],
    templateUrl: './header.component.html'
})
export class HeaderComponent {

    private searchStr: string;
    private dataService: PetientAutoCompleteService;

    constructor(
        private http: Http,
        private router: Router) {
        this.dataService = new PetientAutoCompleteService(http);
    }

    @Output() clickShowSidebar = new EventEmitter<boolean>();
    @Output() clickShowNotifybar = new EventEmitter<boolean>();

    onPatientSelected(selected: CompleterItem) {
        if (selected != null) {
            this.router.navigate(['/patients', selected.originalObject.id]);
        }
    }

    onClickSidebarBtn() {
        this.clickShowSidebar.emit(true);
    }

    onClickNotifybarBtn()  {
        this.clickShowNotifybar.emit(true);
    }
}