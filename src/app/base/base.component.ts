import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import { ViewportService } from './shared/viewport.service';
import {Observable} from "rxjs";
import {ToasterService} from "./toaster/toaster.service";
import {NetworkService} from "../shared/network.service";

@Component({
    selector: 'base',
    encapsulation: ViewEncapsulation.None,
    styleUrls: [
        './base.component.scss'
    ],
    templateUrl: './base.component.html'
})
export class BaseComponent  implements OnInit {
    constructor(
        private viewportService: ViewportService,
        private toaster: ToasterService,
        private network: NetworkService) {
    }

    ngOnInit() {

        let toastId: number;

        this.network.online$.subscribe(is => {
            if (!is) {
                let toast = this.toaster.push('Aplikace je offline.', 0);
                toastId = toast.id;

            } else {
                if (toastId) {
                    this.toaster.pull(toastId);

                }
            }
        });

    }

    onScrollDown () {
        this.viewportService.logScrollDown();
    }

    onScrollUp () {
        this.viewportService.logScrollUp();
    }
}