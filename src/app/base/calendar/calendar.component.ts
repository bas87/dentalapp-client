import { Component, AfterViewInit, ViewChild } from '@angular/core';
import * as jQeury from 'jquery';
import 'fullcalendar';
import 'fullcalendar/dist/locale/cs';
import { AgendaService } from "../../shared/agenda.service";
import { Router } from '@angular/router';


@Component({
  selector: 'calendar',
  styleUrls: ['./calendar.component.scss'],
  templateUrl: './calendar.component.html'
})
export class CalendarComponent implements AfterViewInit {


  constructor(
      private  agendaService: AgendaService,
      private router: Router) {}

  @ViewChild('calendar') calendar;

  ngAfterViewInit() {
    setTimeout(() => {
      jQeury(this.calendar.nativeElement).fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listWeek'
        },
        lang:'cs',
        eventLimit: true,
        aspectRatio: 2,
        defaultView: 'agendaWeek',
        events: (start: any, end: any, timezone: any, callback: any) => {
          this.agendaService.getEvents().subscribe(calendarEvents => callback(calendarEvents));
        },
        eventClick: (event:any) => { this.router.navigate(['/patients', event.patient.id]); }
      });
    }, 100)
  }

}
