import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { routedComponents, CalendarRoutingModule } from './calendar-routing.module';
import { AuthenticationService } from '../../shared/authentication.service';
import { AuthGuard } from '../../shared/auth-guard.service';
import { AgendaService } from '../../shared/agenda.service';
import {PatientService} from "../../shared/patient.service";
import {InsuranceTypeService} from "../../shared/insurance-type.service";
import {InsuranceCompanyService} from "../../shared/insurance-company.service";

@NgModule({
  imports:      [ CommonModule, CalendarRoutingModule ],
  declarations: [ routedComponents ],
  providers: [
      AuthGuard,
      AuthenticationService,
      AgendaService,
      PatientService,
      InsuranceTypeService,
      InsuranceCompanyService
  ]
})
export default class CalendarModule { }