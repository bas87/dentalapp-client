import { NgModule }       from '@angular/core';
import { RouterModule }   from '@angular/router';
import { CalendarComponent } from './calendar.component';
import { AuthGuard } from '../../shared/auth-guard.service';

export const routedComponents = [CalendarComponent];


@NgModule({
  imports: [
      RouterModule.forChild([
    { path: '', component: CalendarComponent, canActivate: [AuthGuard] }
  ])],
  exports: [RouterModule]
})
export class CalendarRoutingModule {}