import { Component, OnInit } from '@angular/core';
import {AgendaService} from "../../../shared/agenda.service";
import {CalendarEvent} from "../../../shared/calendar-event.model";


@Component({
  selector: 'da-todays-agenda',
  styleUrls: [
    './todays-agenda.component.scss'
  ],
  templateUrl: './todays-agenda.component.html'
})
export class TodaysAgendaComponent implements OnInit {

    events: CalendarEvent[] = [];
    loading = false;

    constructor(
        private agendaService: AgendaService) { }

    ngOnInit() {
        this.loading = true;
        
        this.agendaService.getTodaysEvents()
            .subscribe(events => {
                this.events = events;
                this.loading = false;
        });
    }
}