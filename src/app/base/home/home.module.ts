import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { routedComponents, HomeRoutingModule } from './home-routing.module';
import { AuthenticationService } from '../../shared/authentication.service';
import { AuthGuard } from '../../shared/auth-guard.service';
import { TodaysAgendaComponent } from './todays-agenda/todays-agenda.component';
import { PatientService } from '../../shared/patient.service';
import { InsuranceCompanyService } from '../../shared/insurance-company.service';
import { InsuranceTypeService } from '../../shared/insurance-type.service';
import { AgendaService } from "../../shared/agenda.service";

@NgModule({
  imports:      [ CommonModule, HomeRoutingModule ],
  declarations: [ routedComponents, TodaysAgendaComponent ],
  providers: [
    AuthGuard,
    PatientService,
    AgendaService,
    InsuranceCompanyService,
    InsuranceTypeService,
    AuthenticationService
  ]
})
export default class HomeModule { }