import { NgModule }       from '@angular/core';
import { RouterModule }   from '@angular/router';
import { HomeComponent } from './home.component';
import { AuthGuard } from '../../shared/auth-guard.service';

export const routedComponents = [HomeComponent];


@NgModule({
  imports: [
      RouterModule.forChild([
    { path: '', component: HomeComponent, canActivate: [AuthGuard] }
  ])],
  exports: [RouterModule]
})
export class HomeRoutingModule {}